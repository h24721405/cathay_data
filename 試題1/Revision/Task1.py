#!/usr/bin/env python
# coding: utf-8

# In[16]:


pip install selenium


# In[15]:


pip install webdriver_manager


# 1. 開啟Chrome瀏覽器，進到搜尋畫面

# In[ ]:


from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import requests


# In[6]:


#打開Chrome

#method 1
browser = webdriver.Chrome(ChromeDriverManager().install())
browser.get('https://www.ris.gov.tw/info-doorplate/app/doorplate/query')

#method2

#from selenium.webdriver.chrome.options import Options
#設定chrome driver的執行檔路徑
#options = Options()
#options.chrome_executable_path = "chromedriver.exe"

#driver.maximize_window() #視窗最大化
#driver = webdriver.Chrome(options = options)
#driver.get("https://www.ris.gov.tw/info-doorplate/app/doorplate/query")


# In[ ]:


#問題: 1. 進入到有驗證碼的網頁，即可下載驗證碼，再傳入搜尋條件
#     2. 無法直接到驗證碼網頁，需用程式click日期後，再下載驗證碼，再傳入搜尋條件
#     3. method 1 v.s method 2 pros and cons


# 2. 用selenium下載驗證碼Captcha圖片，再用2Captcha提交驗證碼

# In[ ]:


#網頁截圖
from selenium.webdriver.common.by import By

element = driver.find_element(By.ID, "captchaImage_captchaKey")
element.screenshot("gov_captchaKey.png")


# In[ ]:


#success sample:selenium_downlaod_google logo.py
#conditions:Request Method: POST
#result: download google logo as png config


# 3. #進入真正搜尋網址
#    因為是Request method是POST，所以要將搜尋條件包起來，傳給?
#    搜尋條件: 桃園市、桃園區、編釘日期起訖時間

# In[2]:


#import查詢條件
import requests
#url = "https://www.ris.gov.tw/info-doorplate/app/doorplate/inquiry/date"

parameter1 = x  #captchaInput
parameter2 = y  #captchaKey

#POST封包_搜尋條件
search = {
          'searchType': 'date',     #以編釘日期、編釘類別查詢
          'cityCode': '68000000',   #桃園市
          'tkt': '-1',
          'areaCode': '68000010',   #桃園區
          'village': '',
          'neighbor': '',
          'sDate': '111-10-01',     #編釘日期起時間
          'eDate': '111-10-10',     #編釘日期訖時間
          '_includeNoDate': 'on',
          'registerKind': '0',
          'captchaInput': 'ASANH',  #need to use parameter
          'captchaKey': '9d1e20a0a12546c28c77aa219c0ce148' #need to use parameter
         }
res = requests.post("https://www.ris.gov.tw/info-doorplate/app/doorplate/inquiry/date", data = search)
print (res.text)

#marked
#data = {"searchType": "date",    #以編釘日期、編釘類別查詢
#       "cityCode": "68000000",  #桃園市
#       "tkt": "-1",
#       "areaCode": "68000010",  #桃園區
#       "village": "",
#       "neighbor":"",
#       "sDate": "111-10-01",    #編釘日期起時間
#       "eDate": "111-10-10",    #編釘日期訖時間
#       "_includeNoDate": "on",
#       "registerKind": "0",
#       "captchaInput": "SJZN3",
#       "captchaKey": "febae2b36a964be7a2cd7b441819afa5",
#       "floor":"", 
#       "lane": "",
#       "alley": "",
#       "number": "",
#       "number1": "",
#       "ext": "",
#       "_search": "false",
#       "nd": "1665240502426",
#       "rows": "50",
#       "page": "1",
#       "sidx": "",
#       "sord": "asc"
#}
#data = req.post(url, data = data)
#data.encoding = "utf-8"


# 4. 將原始的JSON資料，解析成字典/列表的表示形式

# In[3]:


import json

data = json.loads(res.content.decode('utf-8'))
data
#json.loads(data.text)


# 5.取得row階層下的門牌資訊

# In[ ]:


for key in data["rows"]:
    print(key) #印出v1,v2,v3資料


# In[ ]:


#success sample:request_post_HightSpeedRail.py
#conditions:Request Method: transfer JSON DataFrame to dictionary/list
#result: get time schedule


# 6.關閉Chrome

# In[ ]:


driver.close()

