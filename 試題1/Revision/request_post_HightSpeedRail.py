#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#ref:https://www.youtube.com/watch?v=9Z9xKWfNo7k


# In[22]:


import requests


# In[23]:


payload = {
           'SearchType': 'S',
           'Lang': 'TW',
           'StartStation': 'NanGang',
           'EndStation': 'ZuoYing',
           'OutWardSearchDate': '2022/10/19',
           'OutWardSearchTime': '22:00',
           'ReturnSearchDate': '2022/10/19',
           'ReturnSearchTime': '22:00',
           'DiscountType': ''
           }
res = requests.post("https://www.thsrc.com.tw/TimeTable/Search", data = payload)
print (res.text)


# In[24]:


#ref:https://www.youtube.com/watch?v=IMOUf4BYTG8
import json


# In[26]:


#data = json.loads(res)
data = json.loads(res.content.decode('utf-8'))
data


# In[35]:


#ref:https://bobbyhadz.com/blog/python-typeerror-list-indices-must-be-integers-or-slices-not-dict
for key in data["data"]["DepartureTable"]["TrainItem"]:
    post = key["DepartureTime"]
    print(post)

