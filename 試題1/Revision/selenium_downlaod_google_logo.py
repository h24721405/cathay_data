#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#ref:https://www.youtube.com/watch?v=M4zu9_HIUHI
#ref:https://www.youtube.com/watch?v=hF-dJj559ug
#ref:https://www.youtube.com/watch?v=SjbFxzDgVVc


# In[2]:


pip install selenium


# In[2]:


#載入selenium相關模組
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By


# In[3]:


#設定chrome driver的執行檔路徑
options = Options()
options.chrome_executable_path = "chromedriver.exe"
#建立driver物件實體，用程式操作瀏覽器運作
#driver.maximize_window() #視窗最大化
driver = webdriver.Chrome(options = options)
driver.get("https://www.google.com/")

#螢幕截圖
element = driver.find_element(By.CLASS_NAME, "lnXdpd")
element.screenshot("google_logo.png")
driver.close()


# In[19]:


element.location


# In[20]:


element.size


# In[22]:


left = element.location['x']
right = element.location['x']+element.size['width']
top = element.location['y']
bottom = element.location['y']+element.size['height']
left, right, top, bottom

